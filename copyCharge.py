from colorama import init
from random import randint
from time import sleep
from threading import Thread
import argparse
import os
import sys

# Colorama
init()

# Settings
WIDTH_TEXT = int(len("Replicating successfully done!") / 2) + 3
HEIGHT_TEXT = int(6 / 2)
SIZE_UPDATE_SPEED = 50
INITIAL_FILL = False
MID_SLEEP_TIME = 0.2
CARD_UPDATE_SLEEP_TIME = 1
CENSORED_ID = False
TOP_DOWN_MATRIX = True
NEW_MATRIX_CHAR_CHANCE = 0.05
FOL_MATRIX_CHAR_CHANCE = 0.7
TOP_DOWN_MATRIX_SLEEP_TIME = 0.025
TOP_DOWN_RANDOMIZE_CHARS = True
MID_CORNER_CHAR = '+'
DEBUG = False

KILL_PROG = False

# Possible matrix chars
POS_CHARS = []
for i in range(10):
    POS_CHARS.append(str(i))
for i in range(ord('Z') - ord('A') + 1):
    POS_CHARS.append(chr(ord('A') + i))

class CopyCharge:

    matrix = [[]]
    b_print_matrix = False
    mid_text = ""

    def start_copy(self):
        self._run_matrix()

        self._print_mid("Lay card on reader...")

        card_id = self.get_card_id()

        show_id = card_id
        if CENSORED_ID:
            show_id = self.get_censored_id(card_id)

        # Dump card
        self._print_mid("ID %s saved!" % (show_id))
        sleep(3)

        # Switch card
        self._print_mid("Lay new card on reader...")

        new_card_id = card_id
        while card_id == new_card_id:
            new_card_id = self.get_card_id()

        # Write card
        self._print_mid("Wrting %s to card..." % (show_id))
        os.popen("nfc-mfsetuid %s" % ("".join(card_id.split(' ')))).read()

        # Success!
        self._print_mid("Replicating successfully done!")

        sleep(5)
        self._end_matrix()
        sleep(0.5)
        self._clear()

    def start_copy_slow(self):
        self._run_matrix()

        self._print_mid("Lay card on reader...")

        card_id = self.get_card_id()

        show_id = card_id
        if CENSORED_ID:
            show_id = self.get_censored_id(card_id)

        # Dump card
        self._print_mid("Copying %s..." % (show_id))
        os.popen("mfoc -P 500 -O card.dmp").read()
        self._print_mid("Card successfully copied!")
        sleep(4)

        # Switch card
        self._print_mid("Lay new card on reader...")

        new_card_id = card_id
        while card_id == new_card_id:
            new_card_id = self.get_card_id()

        # Write card
        self._print_mid("Wrting %s to card..." % (show_id))
        os.popen("nfc-mfclassic W A card.dmp").read()

        # Success!
        self._print_mid("Replicating successfully done!")

        sleep(5)
        self._end_matrix()
        sleep(0.5)
        self._clear()

    def get_card_id(self):
        while True:
            nfc_out = os.popen("nfc-list").read()

            if "UID" in nfc_out:
                id_index = nfc_out.index("UID")

                while id_index < len(nfc_out):
                    if nfc_out[id_index] == ':':
                        id_index += 2
                        break
                    id_index += 1

                card_id = ""
                for i in range(4):
                    for x in range(3):
                        card_id += nfc_out[id_index]
                        id_index += 1
                    id_index += 1

                card_id = card_id[:-1]

                break

            sleep(CARD_UPDATE_SLEEP_TIME)

        return card_id

    def get_censored_id(self, card_id: str) -> str:
        return card_id[:-5] + "XX XX"

    def _get_window_size(self):
        if DEBUG:
            return (24, 80)

        size = os.popen('stty size', 'r').read().split()
        size[0] = int(size[0])
        size[1] = int(size[1])

        return size

    def _clear(self):
        print("\033[H\033[J")

    def _run_matrix(self):
        self.b_print_matrix = True

        # Initial fill
        if INITIAL_FILL:
            cur_size = self._get_window_size()
            for x in range(cur_size[1] + 1):
                for y in range(cur_size[0] + 1):
                    if not self._collides_with_text(x, y, cur_size):
                        self.print_at(x, y, POS_CHARS[randint(0, len(POS_CHARS) - 1)])

        # Init matrix
        if TOP_DOWN_MATRIX:
            cur_size = self._get_window_size()
            for y in range(cur_size[0] + 1):
                self.matrix.append([])
                for x in range(cur_size[1] + 1):
                    self.matrix[y].append(' ')

        t = Thread(target=self._print_matrix)
        t.start()

    def _end_matrix(self):
        self.b_print_matrix = False
        sleep(0.1)

    def _print_mid(self, text: str):
        cur_size = self._get_window_size()
        b_col = False
        x_col = 0
        x_col1 = 0
        y_col = 0
        y_col1 = 0
        for x in range(cur_size[1]):
            for y in range(cur_size[0]):
                if self._collides_with_text(x, y, cur_size):
                    if not b_col:
                        b_col = True
                        self.print_at(x, y, MID_CORNER_CHAR)

                        x_col1 = x
                        x_col = cur_size[1] - x
                        while not self._collides_with_text(x_col, y, cur_size):
                            x_col -= 1

                        self.print_at(x_col, y, MID_CORNER_CHAR)

                        y_col1 = y
                        y_col = cur_size[0] - y
                        while not self._collides_with_text(x, y_col, cur_size):
                            y_col -= 1

                        self.print_at(x, y_col, MID_CORNER_CHAR)

                        self.print_at(x_col, y_col, MID_CORNER_CHAR)

                    elif not ((x == x_col1 and y == y_col1) or (x == x_col1 and y == y_col)):
                        if not ((x == x_col and y == y_col) or (x == x_col and y == y_col1)):
                            self.print_at(x, y, ' ')

        if self.mid_text == "":
            self.mid_text = text
            t = Thread(target=self._print_mid_thread)
            t.start()

        else:
            self.mid_text = text

    def _print_mid_thread(self):
        bold_char = 0
        while self.b_print_matrix and not KILL_PROG:
            cur_size = self._get_window_size()

            x = cur_size[1] / 2 - len(self.mid_text) / 2
            y = cur_size[0] / 2

            text = self.mid_text[:bold_char] + self.mid_text[bold_char].upper()

            if bold_char != len(self.mid_text) - 1:
                text += self.mid_text[bold_char + 1:]

            while True:
                bold_char = (bold_char + 1) % len(self.mid_text)
                if self.mid_text[bold_char].isalpha():
                    break

            self.print_at(int(x), int(y), text, style="[1;34m")

            sleep(MID_SLEEP_TIME)

    def print_at(self, x: int, y: int, text: str, style="[0;32m"):
        sys.stdout.write("\x1b7\x1b[%d;%df\033%s%s\033[4m\x1b8" % (y, x, style, text))
        sys.stdout.flush()

    def _print_matrix(self):
        if TOP_DOWN_MATRIX:
            cur_size = self._get_window_size()
            while self.b_print_matrix and not KILL_PROG:
                for y in range(cur_size[0] + 1):
                    for x in range(cur_size[1] + 1):
                        if y != cur_size[0]:
                            if not self._collides_with_text(x, cur_size[0] - y, cur_size):
                                if TOP_DOWN_RANDOMIZE_CHARS:
                                    if self.matrix[cur_size[0] - y][x] != ' ':
                                        self.print_at(x, cur_size[0] - y, self._rand_char())
                                    else:
                                        self.print_at(x, cur_size[0] - y, ' ')
                                else:
                                    self.print_at(x, cur_size[0] - y, self.matrix[cur_size[0] - y][x])

                            if y != cur_size[0]:
                                self.matrix[cur_size[0] - y][x] = self.matrix[cur_size[0] - y - 1][x]

                for x in range(cur_size[1] + 1):
                    chance = FOL_MATRIX_CHAR_CHANCE
                    if self.matrix[1][x] == ' ':
                        chance = NEW_MATRIX_CHAR_CHANCE

                    if randint(1, 100) < 100 * chance:
                        self.matrix[0][x] = self._rand_char()
                    else:
                        self.matrix[0][x] = ' '

                sleep(TOP_DOWN_MATRIX_SLEEP_TIME)

        else:
            i = 0
            while self.b_print_matrix and not KILL_PROG:
                if i == 0:
                    cur_size = self._get_window_size()
                    i = SIZE_UPDATE_SPEED
                else:
                    i -= 1

                x = randint(1, cur_size[1])
                y = randint(1, cur_size[0])

                if not self._collides_with_text(x, y, cur_size):
                    self.print_at(x, y, self._rand_char())

    def _rand_char(self):
        return POS_CHARS[randint(0, len(POS_CHARS) - 1)]

    def _collides_with_text(self, x: int, y: int, cur_size: tuple):
        if x > int(cur_size[1] / 2 - WIDTH_TEXT) and x < int(cur_size[1] / 2 + WIDTH_TEXT):
            if y > int(cur_size[0] / 2 - HEIGHT_TEXT) and y < int(cur_size[0] / 2 + HEIGHT_TEXT):
                return True

class _Getch:
    """Gets a single character from standard input. Does not echo to the screen."""
    def __init__(self):
        try:
            self.impl = _GetchWindows()
        except ImportError:
            self.impl = _GetchUnix()

    def __call__(self): return self.impl()


class _GetchUnix:
    def __init__(self):
        import tty, sys

    def __call__(self):
        import sys, tty, termios
        fd = sys.stdin.fileno()
        old_settings = termios.tcgetattr(fd)
        try:
            tty.setraw(sys.stdin.fileno())
            ch = sys.stdin.read(1)
        finally:
            termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
        return ch


class _GetchWindows:
    def __init__(self):
        import msvcrt

    def __call__(self):
        import msvcrt
        return msvcrt.getch()


get_char = _Getch()

if __name__ == "__main__":
    copyCharge = CopyCharge()

    parser = argparse.ArgumentParser(description="CopyCharge")
    parser.add_argument("-a", "--all", help="Does a complete copy of the targeted card", action="store_true")
    parser.add_argument("-i", "--initial_fill", help="Fills the complete matrix at the beginning", action="store_true")
    parser.add_argument("-c", "--censor_id", help="Hides the last two bytes of the ID", action="store_true")
    parser.add_argument("-r", "--random_matrix", help="Another matrix style", action="store_true")
    parser.add_argument("-d", "--debug", help="Ready for debugging", action="store_true")

    args = parser.parse_args()
    if args.initial_fill:
        INITIAL_FILL = True
    if args.censor_id:
        CENSORED_ID = True
    if args.random_matrix:
        TOP_DOWN_MATRIX = False
    if args.debug:
        DEBUG = True

    try:
        if args.all:
            copyCharge.start_copy_slow()
        else:
            copyCharge.start_copy()

    except KeyboardInterrupt:
        KILL_PROG = True
        sleep(0.3)
        CopyCharge()._clear()
        exit(0)